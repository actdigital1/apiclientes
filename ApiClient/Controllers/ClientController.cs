using Microsoft.AspNetCore.Mvc;
using ApiClient.Models;
using ApiClient.Services;


namespace ApiClient.Controllers;

[ApiController]
[Route("api/[controller]")]
public class ClientController : ControllerBase
{

    private readonly ClientsService _clientService;

    public ClientController(ClientsService clientService) => _clientService = clientService;

    [HttpGet]
    public async Task<List<Client>> Get() => await _clientService.GetAsync();

    [HttpGet("{id:length(24)}")]
    public async Task<ActionResult<Client>> Get(string id)
    {
        var client = await _clientService.GetAsync(id);

        if (client is null)
        {
            return NotFound();
        }

        return client;
    }

    [HttpPost]
    public async Task<IActionResult> Post(Client newClient)
    {
        await _clientService.CreateAsync(newClient);

        return CreatedAtAction(nameof(Get), new { id = newClient.Id, }, newClient);
    }

    [HttpPut("{id:length(24)}")]
    public async Task<IActionResult> Update(string id, Client updateClient)
    {
        var client = await _clientService.GetAsync(id);

        if (client is null)
        {
            return NotFound();
        }

        updateClient.Id = client.Id;
        await _clientService.UpdateAsync(id, updateClient);

        return NoContent();
    }

    [HttpDelete("{id:length(24)}")]
    public async Task<IActionResult> Delete(string id)
    {
        var client = await _clientService.GetAsync(id);

        if (client is null)
        {
            return NotFound();
        }

        await _clientService.DeleteAsync(id);

        return NoContent();
    }
}