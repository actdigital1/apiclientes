using ApiClient.Models;
using ApiClient.Services;


var builder = WebApplication.CreateBuilder(args);

// builder.Services.AddCors(options =>
// {
//     options.AddPolicy(name: "MyPolicy",
//                 policy =>
//                 {
//                     policy.WithOrigins("http://example.com",
//                         "http://www.contoso.com",
//                         "https://cors1.azurewebsites.net",
//                         "https://cors3.azurewebsites.net",
//                         "https://localhost:3000",
//                         "https://localhost:5001")
//                             .WithMethods("PUT", "DELETE", "GET");
//                 });
// });

// Add services to the container.
builder.Services.Configure<ClientDBSettings>(
    builder.Configuration.GetSection("ClientStoreCosmoDB"));
    builder.Services.AddSingleton<ClientsService>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors(x => x
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials()
            //.WithOrigins("https://localhost:44351")); // Allow only this origin can also have multiple origins seperated with comma
            .SetIsOriginAllowed(origin => true));// Allow any origin

app.UseAuthorization();

app.MapControllers();

app.Run();
