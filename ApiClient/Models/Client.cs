using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ApiClient.Models;

public class Client
{

    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string? Id { get; set; }

    [BsonElement("Name")]
    public string? Name { get; set; }
    public string? LastName { get; set; }
    public string? BirthDate { get; set; }
    public string? State { get; set; }
    public string? BirthCity { get; set; }
}