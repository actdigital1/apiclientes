namespace ApiClient.Models;
public class ClientDBSettings {

    public string? ConnectionString { get; set; }
    public string? DatabaseName { get; set; }
    public string? CollectionName { get; set; }

}