

using MongoDB.Driver;
using Microsoft.Extensions.Options;
using ApiClient.Models;

namespace ApiClient.Services;

public class ClientsService
{

    private readonly IMongoCollection<Client> _clientCollection;

    public ClientsService(IOptions<ClientDBSettings> mongoDBSettings)
    {
        MongoClient client = new MongoClient(mongoDBSettings.Value.ConnectionString);
        IMongoDatabase database = client.GetDatabase(mongoDBSettings.Value.DatabaseName);
        _clientCollection = database.GetCollection<Client>(mongoDBSettings.Value.CollectionName);
    }

    public async Task<List<Client>> GetAsync() => await _clientCollection.Find(_ => true).ToListAsync();
    public async Task<Client?> GetAsync(string id) => await _clientCollection.Find(x => x.Id == id).FirstOrDefaultAsync();
    public async Task CreateAsync(Client neweClient) => await _clientCollection.InsertOneAsync(neweClient);
    public async Task UpdateAsync(string id, Client updateClient) => await _clientCollection.ReplaceOneAsync(x => x.Id == id, updateClient);
    public async Task DeleteAsync(string id) => await _clientCollection.DeleteOneAsync(x => x.Id == id);

}